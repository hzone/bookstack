#!/bin/bash
###################################################################################
#   ██     ██  █████  ██      ██ ██████                                           #
#   ██     ██ ██   ██ ██      ██ ██   ██                                          #
#   ██  █  ██ ███████ ██      ██ ██   ██                                          #
#   ██ ███ ██ ██   ██ ██      ██ ██   ██                                          #
#    ███ ███  ██   ██ ███████ ██ ██████                                           #
#                                                                                 #
#                      walid@ettayeb.fr                                           #
#                                                                                 #
# Nom		    : Bookstack Installer                                             #
# Description	: Script qui permet d'installer automatiquement BookStack         #
# Param 1	    :                                                                 #
# Param 2    	:                                                                 #
#                                                                                 #
# Exemple   : ./5SRC1.NGUYEN.Davy.post-install.sh nimda p@ssw.rD                  #
# Auteur    : Walid ETTAYEB                                                       #
# Email		: walid@ettayeb.fr                                                    #
# Changelog	:                                                                     #
# Version	: 1                                                                   #
# Changelog	: Creation du script                                                  #
#                                                                                 #
#                                                                                 #
###################################################################################


# Récupère le domaine à utiliser à partir du premier paramètre fourni,
# Sinon, demander à l'utilisateur de saisir son domaine
DOMAIN=$1
if [ -z "$1" ]
then
echo ""
printf "Entrez le domaine dans lequel vous voulez héberger BookStack et appuyez sur [ENTER]\nExemples: ettayeb.fr or docs.ettayeb.fr\n"
read -r DOMAIN
fi

# Vérifie qu'un domaine a été fourni, sinon affiche
# un message d'erreur et arrêter le script
if [ -z "$DOMAIN" ]
then
  >&2 echo 'ERROR: Un domaine doit être fourni pour exécuter ce script'
  exit 1
fi

# Obtenir l'adresse IP de la machine actuelle
CURRENT_IP=$(ip addr | grep 'state UP' -A3 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')




# =========================================================
# =============== Intallation des prérequis ===============
# =========================================================


# Installation des paquets système
export DEBIAN_FRONTEND=noninteractive
apt-get install -y software-properties-common
#add-apt-repository universe
apt update
apt install -y git unzip apache2 php7.4 curl php7.4-fpm php7.4-curl php7.4-mbstring php7.4-ldap \
php7.4-tidy php7.4-xml php7.4-zip php7.4-gd php7.4-mysql mariadb-server libapache2-mod-php7.4

# Configuration de la base de donnée
DB_PASS="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)"
mysql -u root --execute="CREATE DATABASE bookstack;"
mysql -u root --execute="CREATE USER 'bookstack'@localhost IDENTIFIED BY '$DB_PASS';"
mysql -u root --execute="GRANT ALL ON bookstack.* TO 'bookstack'@'localhost';FLUSH PRIVILEGES;"


# Téléchargement de BookStack
cd /var/www || exit
git clone https://github.com/BookStackApp/BookStack.git --branch release --single-branch bookstack
BOOKSTACK_DIR="/var/www/bookstack"
cd $BOOKSTACK_DIR || exit

# Installation de composer
EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

### Vérification du checksum du script pour des raisons de sécurité.
if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
    >&2 echo 'ATTENTION : Le fichier que vous avez télécharger est potentiellement malveillant '
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
rm composer-setup.php

# Généralisation de Composer
mv composer.phar /usr/local/bin/composer

# Installation des dépendances de BookStack 
export COMPOSER_ALLOW_SUPERUSER=1
php /usr/local/bin/composer install --no-dev --no-plugins

# Mise à jour des variables d'environement de BookStack
cp .env.example .env
sed -i.bak "s@APP_URL=.*\$@APP_URL=http://$DOMAIN@" .env
sed -i.bak 's/DB_DATABASE=.*$/DB_DATABASE=bookstack/' .env
sed -i.bak 's/DB_USERNAME=.*$/DB_USERNAME=bookstack/' .env
sed -i.bak "s/DB_PASSWORD=.*\$/DB_PASSWORD=$DB_PASS/" .env

# Generation d'une clée pour l'application
php artisan key:generate --no-interaction --force
# Seeding de la base de donnée
php artisan migrate --no-interaction --force

# Mise à jour des permissions sur les fichiers et dossiers de l'application
chown www-data:www-data -R bootstrap/cache public/uploads storage && chmod -R 755 bootstrap/cache public/uploads storage

# Paramétrage d'apache
a2enmod rewrite
a2enmod php7.4

cat >/etc/apache2/sites-available/bookstack.conf <<EOL
<VirtualHost *:80>
	ServerName ${DOMAIN}

	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/bookstack/public/

    <Directory /var/www/bookstack/public/>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
        <IfModule mod_rewrite.c>
            <IfModule mod_negotiation.c>
                Options -MultiViews -Indexes
            </IfModule>

            RewriteEngine On

            # Handle Authorization Header
            RewriteCond %{HTTP:Authorization} .
            RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

            # Redirect Trailing Slashes If Not A Folder...
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{REQUEST_URI} (.+)/$
            RewriteRule ^ %1 [L,R=301]

            # Handle Front Controller...
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^ index.php [L]
        </IfModule>
    </Directory>

	ErrorLog \${APACHE_LOG_DIR}/error.log
	CustomLog \${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
EOL

a2dissite 000-default.conf
a2ensite bookstack.conf

# Restart apache to load new config
systemctl restart apache2

echo ""
echo "Installation terminée, votre instance de BookStack devrait maintenant être installée."
echo "Vous pouvez vous connecter avec l'email 'admin@admin.com' et le mot de passe 'password'."
echo ""
echo "Vous pouvez accéder à votre instance BookStack à l'adresse suivante : http://$CURRENT_IP/ ou http://$DOMAIN/."